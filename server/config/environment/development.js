'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/spi-dev'
  },

  // Seed database on startup
  seedDB: true

};
